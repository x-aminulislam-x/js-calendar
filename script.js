// https://jsfiddle.net/niinpatel/xbdrr3q2/
// function to generate whole years date in array
const getAllDaysInYear = (year) => {
  const month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  return month.map((m) =>
    Array.from(
      { length: new Date(year, m, 0).getDate() }, // get next month, zeroth's (previous) day
      (_, i) => new Date(year, m - 1, i + 1) // get current month (0 based index)
    )
  );
};

const allDatesInYear = getAllDaysInYear(2023);

let currentMonth = 0;

for (let i = 0; i < allDatesInYear.length; i++) {
  for (let j = 0; j < allDatesInYear[i].length; j++) {
    if (
      new Date(allDatesInYear[i][j]).toLocaleString("default", {
        month: "long",
      }) ===
      new Date().toLocaleString("default", {
        month: "long",
      })
    ) {
      currentMonth = i;
      break;
    }
  }
}

const calender = document.querySelector(".calendar-main");
const div = document.createElement("div");
div.classList.add("month");

const handlePreMonth = () => {
  if (currentMonth <= 0) return;
  dd(currentMonth--);
};
const handleNextMonth = () => {
  if (currentMonth >= allDatesInYear.length - 1) return;
  dd(currentMonth++);
};

const tableHeaderTH = document.querySelectorAll(".calendar-head tr th");

const splitArray = (arr, rows) => {
  // to adjust the date with week day
  let dateIndx = 0;
  for (let i = 0; i < 7; i++) {
    if (
      tableHeaderTH[dateIndx].innerText ===
      arr[i].toLocaleString("default", { weekday: "short" })
    ) {
      break;
    } else {
      // unshifting null to the start of the array
      arr.unshift(null);
      dateIndx++;
    }
  }

  // making 2D array according to weeks
  const itemsPerRow = Math.ceil(arr.length / rows);
  return arr.reduce((acc, val, ind) => {
    const currentRow = Math.floor(ind / rows);
    if (!acc[currentRow]) {
      acc[currentRow] = [val];
    } else {
      acc[currentRow].push(val);
    }
    return acc;
  }, []);
};
console.log(currentMonth);

function dd(cur) {
  console.log(cur);
  const table = document.querySelector(".calendar-table");
  const tableBody = document.querySelector(".calendar-body");
  const monthName = document.querySelector(".calendar-month-name");

  let month = allDatesInYear[cur];

  tableBody.innerHTML = "";

  const splitWeeks = splitArray(month, 7);
  console.log(splitWeeks);
  monthName.innerText = new Date(splitWeeks[0][0]).toLocaleString("default", {
    month: "long",
  });
  splitWeeks.map((m) => {
    const tr = document.createElement("tr");
    m.map((d, indx) => {
      const td = document.createElement("td");
      if (d) {
        td.innerText = d.toLocaleDateString([], {
          day: "numeric",
        });
      }
      tr.appendChild(td);
    });
    tableBody.appendChild(tr);
  });

  table.appendChild(tableBody);
}

dd(currentMonth);
